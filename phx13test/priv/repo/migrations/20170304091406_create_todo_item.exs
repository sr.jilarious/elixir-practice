defmodule Phx13test.Repo.Migrations.CreatePhx13test.Todo.Item do
  use Ecto.Migration

  def change do
    create table(:todo_items) do
      add :name, :string
      add :state, :integer

      timestamps()
    end

  end
end
