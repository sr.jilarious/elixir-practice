defmodule Phx13test.Repo.Migrations.CreatePhx13test.Accounts.User do
  use Ecto.Migration

  def change do
    create table(:accounts_users) do
      add :name, :string
      add :email, :string
      add :description, :string

      timestamps()
    end

  end
end
