defmodule Phx13test.TodoTest do
  use Phx13test.DataCase

  alias Phx13test.Todo
  alias Phx13test.Todo.Item

  @create_attrs %{name: "some name", state: 42}
  @update_attrs %{name: "some updated name", state: 43}
  @invalid_attrs %{name: nil, state: nil}

  def fixture(:item, attrs \\ @create_attrs) do
    {:ok, item} = Todo.create_item(attrs)
    item
  end

  test "list_items/1 returns all items" do
    item = fixture(:item)
    assert Todo.list_items() == [item]
  end

  test "get_item! returns the item with given id" do
    item = fixture(:item)
    assert Todo.get_item!(item.id) == item
  end

  test "create_item/1 with valid data creates a item" do
    assert {:ok, %Item{} = item} = Todo.create_item(@create_attrs)
    
    assert item.name == "some name"
    assert item.state == 42
  end

  test "create_item/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Todo.create_item(@invalid_attrs)
  end

  test "update_item/2 with valid data updates the item" do
    item = fixture(:item)
    assert {:ok, item} = Todo.update_item(item, @update_attrs)
    assert %Item{} = item
    
    assert item.name == "some updated name"
    assert item.state == 43
  end

  test "update_item/2 with invalid data returns error changeset" do
    item = fixture(:item)
    assert {:error, %Ecto.Changeset{}} = Todo.update_item(item, @invalid_attrs)
    assert item == Todo.get_item!(item.id)
  end

  test "delete_item/1 deletes the item" do
    item = fixture(:item)
    assert {:ok, %Item{}} = Todo.delete_item(item)
    assert_raise Ecto.NoResultsError, fn -> Todo.get_item!(item.id) end
  end

  test "change_item/1 returns a item changeset" do
    item = fixture(:item)
    assert %Ecto.Changeset{} = Todo.change_item(item)
  end
end
