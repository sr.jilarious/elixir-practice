defmodule Phx13test.Todo.Item do
  use Ecto.Schema
  
  schema "todo_items" do
    field :name, :string
    field :state, :integer

    timestamps()
  end
end
