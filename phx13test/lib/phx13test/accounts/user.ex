defmodule Phx13test.Accounts.User do
  use Ecto.Schema
  
  schema "accounts_users" do
    field :name, :string
    field :email, :string
    field :description, :string

    timestamps()
  end
end
