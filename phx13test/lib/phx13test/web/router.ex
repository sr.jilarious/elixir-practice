defmodule Phx13test.Web.Router do
  use Phx13test.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Phx13test.Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/hi", HelloController, :index
    get "/hi/:caller", HelloController, :show

  end

  # Other scopes may use custom stacks.
  # scope "/api", Phx13test.Web do
  #   pipe_through :api
  # end
end
