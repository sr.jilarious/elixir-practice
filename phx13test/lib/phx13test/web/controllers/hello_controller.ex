defmodule Phx13test.Web.HelloController do
    use Phx13test.Web, :controller

    def index(conn, _params) do
        render conn, "index.html"
    end

    def show(conn, %{"caller" => caller}) do
        render conn, "show.html", caller: caller
    end
end