defmodule Phx13test.Web.PageController do
  use Phx13test.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
