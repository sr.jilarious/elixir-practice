# Test creating a simple process to receive a message

parent = self()

defmodule Proc do
    def client do
        receive do

            {:hello, pid, msg} ->
                IO.puts("Got hi message from #{inspect pid}: '#{msg}'")
                send(pid, {:response, "pong"})
                client()
            {:boob, pid, msg} ->
                IO.puts "Got hi! #{msg}"
                send(pid, {:response, "pong"})
                client()
            #after 3_000 ->
            #    IO.puts("Timeout!")
        end
    end

    def server do
        receive do
            {:response, msg} ->
                IO.puts("Received response: '#{msg}'!")
                server()
            #after 3_000 -> IO.puts("No response!!")
        end
    end

    def input_func(client_pid, server_pid) do
        IO.puts "Press 'q' to quit, anything else to send message."
        c = IO.gets("")
        if c != "q\n" do
            send client_pid, {:hello, server_pid, "temp"}
            send client_pid, {:boob, server_pid, "Hello"}
            input_func(client_pid, server_pid)
        end
    end
end

# Spawn a child process to listen for incoming messages and respond.
pid = spawn fn ->
    IO.puts "Process spawned!"
    Proc.client()
    IO.puts "Client process done!"
end

# Spawn a listener to respond to the child response
server_pid = spawn fn ->
    IO.puts "Server Process spawned!"
    Proc.server()
    IO.puts "Server process done!"
end


# Send our child some messages, to which we expect responses.


#:timer.sleep(1000);

IO.puts "Spawned proc, alive?: #{Process.alive?(pid)}"
IO.puts "Spawned server proc, alive?: #{Process.alive?(server_pid)}"

Proc.input_func(pid, server_pid)

IO.puts "Finished"

