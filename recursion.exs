# Testing recursion

defmodule RecursionTest do

    def print_mul(msg, n) when n <= 1 do
        IO.puts(msg)
    end

    def print_mul(msg, n) do
        IO.puts([msg, " ",  Integer.to_string(n)])
        print_mul(msg, n - 1)
    end

    def fib(x) when x == 0 or x == 1 do
        1
    end

    def fib(x) do
       fib(x - 1) + fib(x - 2)
    end
end

RecursionTest.print_mul("Testing !!", 5)

IO.puts RecursionTest.fib(10)