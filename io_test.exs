# Testing file IO

IO.puts "What file would you like to open?"

fileName = IO.gets ""
fileName = fileName |> String.strip

IO.puts ["Trying to read file '", fileName, "'."]

case File.read fileName do
    {:ok, contents} ->
        IO.puts "Contents of file:"
        IO.puts String.duplicate("-", 75)
        IO.puts contents
        IO.puts String.duplicate("-", 75)

    {:error, reason} ->
        case reason do
            # File doesn't exist
            :enoent ->
                IO.puts ["File '", fileName, "' does not exist."]
            # Default case
            _ ->
                IO.puts ["Could not open file '", fileName, "', reason: '", to_string(reason), "'."]
        end
end
